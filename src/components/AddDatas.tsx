import { useState } from "react";
import { Form, Button } from "react-bootstrap";
import userService from "./Service";

const AddDatas = () => {
  const [id, setId] = useState<number>(0);
  const [user, setUser] = useState<number>(0);
  const [title, setTitle] = useState<string>("");

  const handleUserid = (event: any): void => {
    setUser(event.target.value);
  };

  const handleId = (event: any): void => {
    setId(event.target.value);
  };

  const handleTitle = (event: any): void => {
    setTitle(event.target.value);
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    const userdata = {
      id: id,
      user: user,
      title: title,
    };
    createUser(userdata);
  };

  const createUser = (data: any) => {
    userService
      .Create(data)
      .then((response: any) => {
        alert("succuessfully submitted")
      })
      .catch((error) => {
        throw error;
      });
  };

  return (
    <div>
      <Form className="w-25 mx-auto mt-5">
        <Form.Group className="mb-3">
          <Form.Label>UserId</Form.Label>
          <Form.Control
            type="number"
            name="userid"
            placeholder="Enter userId..."
            value={user}
            onChange={(event) => handleUserid(event)}
          />
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Id</Form.Label>
          <Form.Control
            type="number"
            name="id"
            placeholder="enter Id.."
            value={id}
            onChange={(event) => handleId(event)}
          />
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            name="title"
            placeholder="enter Title...."
            value={title}
            onChange={(event) => handleTitle(event)}
          />
        </Form.Group>
        <Button
          variant="primary"
          type="submit"
          onClick={(event) => handleSubmit(event)}
        >
          Submit
        </Button>
      </Form>
    </div>
  );
};

export default AddDatas;
