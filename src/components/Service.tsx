import axios from "axios";

const baseUrl = "https://jsonplaceholder.typicode.com/todos";

const getAll = () => {
  return axios.get(baseUrl);
};

const Delete = (id: any) => {
  return axios.delete(baseUrl + "/" + id);
};

const Create = (data: any) => {
  return axios.post(baseUrl + "/", data);
};

const userService = {
  getAll,
  Delete,
  Create,
};

export default userService;
