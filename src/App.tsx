import "bootstrap/dist/css/bootstrap.css";
import "./App.css";
import { Navbar, Container, Nav, NavbarBrand } from "react-bootstrap";
import AddDatas from "./components/AddDatas";
import Home from "./components/Home";
import { Routes, Route, NavLink } from "react-router-dom";
const App = () => {
  return (
    <div className="container-fluid">
      <Navbar bg="primary" variant="dark">
        <Container>
          <NavbarBrand>CrudApp</NavbarBrand>
          <Nav className="me-auto navbar-light">
            <NavLink className="active one" to="/">
              Home
            </NavLink>
            <NavLink className="active one" to="/postedList">
              AddUser
            </NavLink>
          </Nav>
        </Container>
      </Navbar>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/PostedList" element={<AddDatas />} />
      </Routes>
    </div>
  );
};

export default App;
