import { useEffect, useState } from "react";
import { Table, Row, Col, Button } from "react-bootstrap";
import userService from "./Service";
import { Todo } from "./model";

const Home = () => {
  const [data, setdata] = useState<Todo[]>([]);

  const listData = () => {
    userService
      .getAll()
      .then((response: any) => {
        const myData: Todo[] = response.data;
        setdata(myData);
      })
      .catch((error) => {
        throw error;
      });
  };

  useEffect(() => {
    listData();
  }, []);

  const deleteData = (id: any) => {
    userService
      .Delete(id)
      .then((response: any) => {
        listData();
      })
      .catch((error) => {
        throw error;
      });
  };

  return (
    <div className="w-50 mx-auto mt-5">
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>UserId</th>
            <th>id</th>
            <th>title</th>
            <th>delete item</th>
          </tr>
        </thead>
        <tbody>
          {data.map((user: Todo) => (
            <tr key={user.id}>
              <td>{user.userId}</td>
              <td>{user.id}</td>
              <td>{user.title}</td>
              <td className="col-2">
                <Row>
                  <Col md>
                    <Button
                      onClick={() => deleteData(user.id)}
                      className="btn btn-primary"
                    >
                      Delete
                    </Button>
                  </Col>
                </Row>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Home;
